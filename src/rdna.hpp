#include <iostream>
#include <random>
#include <string>

using namespace std;
using std::string;

string randDNA(int seed, string bases, int n)
{
 random_device rd;
 mt19937 eng1(seed);
 
 int min =0;
 int value = 0; 
 int max = bases.size()-1;;
 uniform_int_distribution<> uniform(min,max);
 string dna = ""; 


 for(int i=0 ; i<n ; i++)
{
	value = uniform(eng1);
	dna = dna + bases[value];
}

 return dna;
}
